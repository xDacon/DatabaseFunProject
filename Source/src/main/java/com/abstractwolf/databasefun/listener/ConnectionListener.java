package com.abstractwolf.databasefun.listener;

import com.abstractwolf.databasefun.DatabaseFun;
import com.abstractwolf.databasefun.util.Database;
import com.abstractwolf.databasefun.util.Style;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by ThatAbstractWolf on 2017-06-07.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class ConnectionListener implements Listener {

	private DatabaseFun plugin;

	public ConnectionListener(DatabaseFun plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onConnect(PlayerJoinEvent event) {

		Player player = event.getPlayer();
		Database database = plugin.getDatabaseInstance();

		if (!database.isClosed()) {
			plugin.getUserManager().addUser(player.getUniqueId());
			plugin.getUserManager().loadUser(player);
		}

		event.setJoinMessage(Style.getPrefix("Join") + ChatColor.RED + player.getName() + ChatColor.GRAY + " joined the server!");
	}

	@EventHandler
	public void onDisconnect(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		Database database = plugin.getDatabaseInstance();

		if (!database.isClosed()) {
			plugin.getUserManager().saveUser(player.getUniqueId(), false);
			plugin.getUserManager().removeUser(player.getUniqueId());
		}

		event.setQuitMessage(Style.getPrefix("Leave") + ChatColor.RED + player.getName() + ChatColor.GRAY + " left the server!");
	}
}
