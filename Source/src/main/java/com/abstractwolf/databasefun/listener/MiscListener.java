package com.abstractwolf.databasefun.listener;

import com.abstractwolf.databasefun.DatabaseFun;
import com.abstractwolf.databasefun.base.Rank;
import com.abstractwolf.databasefun.base.User;
import com.abstractwolf.databasefun.util.Database;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Optional;

/**
 * Created by ThatAbstractWolf on 2017-06-07.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class MiscListener implements Listener {

	private DatabaseFun plugin;

	public MiscListener(DatabaseFun plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {

		Player player = event.getPlayer();

		Optional<User> user = plugin.getUserManager().getUser(player.getUniqueId());

		if (user.isPresent()) {

			User foundUser = user.get();

			if (!foundUser.getRank().equals(Rank.PLAYER)) {
				event.setFormat(foundUser.getRank().getFormattedPrefix(false, false) + " " + ChatColor.GRAY + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + event.getMessage());
			} else {
				event.setFormat(ChatColor.GRAY + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + event.getMessage());
			}
		} else {
			event.setFormat(ChatColor.DARK_RED + player.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + event.getMessage());
		}
	}
}