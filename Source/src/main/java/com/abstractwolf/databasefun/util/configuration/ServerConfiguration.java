package com.abstractwolf.databasefun.util.configuration;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.util.FileUtil;

import java.io.File;
import java.io.IOException;

/**
 * Created by ThatAbstractWolf on 2017-06-12.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class ServerConfiguration {

	private final String TEMPLATE_FILES = Bukkit.getWorldContainer().getAbsolutePath() + "/plugins/FileTemplates/";
	private final String SERVER_DATA_FILE_NAME = "serverdata.yml";

	private File file;
	private YamlConfiguration yamlConfiguration;

	private String serverName = "DEFAULT-";

	private String databaseHost = "localhost", databasePort = "3306", databaseDB = "default", databaseUsername = "root", databasePassword = "root";

	private int backupCount = 0;

	public ServerConfiguration() {

		file = new File(Bukkit.getWorldContainer().getAbsolutePath() + "/" + SERVER_DATA_FILE_NAME);

		if (file.exists()) {
			yamlConfiguration = YamlConfiguration.loadConfiguration(file);
			System.out.println("Loaded Server Configuration, '" + file.getName() + "'!");
		} else {
			FileUtil.copy(new File(TEMPLATE_FILES + SERVER_DATA_FILE_NAME), file.getAbsoluteFile());
			System.out.println("Copied " + SERVER_DATA_FILE_NAME + " to " + file.getAbsolutePath());

			yamlConfiguration = YamlConfiguration.loadConfiguration(file);
			System.out.println("Loaded Server Configuration, '" + file.getName() + "'!");
		}
	}

	public void loadData() {

		if (getYamlConfiguration() != null) {

			// Server
			serverName = getYamlConfiguration().getString("serverName");
			backupCount = getYamlConfiguration().getInt("backupCount");

			// Database
			databaseHost = getYamlConfiguration().getString("databaseHost");
			databasePort = getYamlConfiguration().getString("databasePort");
			databaseDB = getYamlConfiguration().getString("databaseDB");
			databaseUsername = getYamlConfiguration().getString("databaseUsername");
			databasePassword = getYamlConfiguration().getString("databasePassword");
		}
	}

	public void saveData() {

		if (getYamlConfiguration() != null) {
			try {
				// Server
				getYamlConfiguration().set("serverName", serverName);

				// Database
				getYamlConfiguration().set("databaseHost", databaseHost);
				getYamlConfiguration().set("databasePort", databasePort);
				getYamlConfiguration().set("databaseDB", databaseDB);
				getYamlConfiguration().set("databaseUsername", databaseUsername);
				getYamlConfiguration().set("databasePassword", databasePassword);

				if (backupCount > 5) {
					getYamlConfiguration().set("backupCount", 0);
				} else {
					getYamlConfiguration().set("backupCount", backupCount + 1);
				}

				getYamlConfiguration().save(file);
				System.out.println("Saved '" + SERVER_DATA_FILE_NAME + "'.");
			} catch (IOException e) {
				System.out.println("Could not save '" + SERVER_DATA_FILE_NAME + "'.");
			}
		}
	}

	public YamlConfiguration getYamlConfiguration() {
		return yamlConfiguration;
	}

	public String getServerName() {
		return serverName;
	}

	public int getBackupCount() {
		return backupCount;
	}

	public String getDatabaseHost() {
		return databaseHost;
	}

	public String getDatabasePort() {
		return databasePort;
	}

	public String getDatabaseDB() {
		return databaseDB;
	}

	public String getDatabaseUsername() {
		return databaseUsername;
	}

	public String getDatabasePassword() {
		return databasePassword;
	}
}
