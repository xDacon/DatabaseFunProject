package com.abstractwolf.databasefun.util.menu;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by ThatAbstractWolf on 2017-06-13.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class MenuManager implements Listener {

	/**
	 * Construct the MenuManager, which also registers the event.
	 * @param plugin - the plugin
	 */
	public MenuManager(JavaPlugin plugin) {
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Validate.notNull(event.getInventory(), "Inventory null.");
		InventoryHolder holder = event.getInventory().getHolder();
		Validate.notNull(holder, "Inventory null.");

		if (!(holder instanceof MenuFactory)) {
			return;
		}

		event.setCancelled(true);

		if (!(event.getWhoClicked() instanceof Player)) {
			return;
		}

		Player player = (Player) event.getWhoClicked();
		MenuFactory menu = (MenuFactory) holder;

		ItemStack clicked = event.getCurrentItem();
		ClickType clickType = event.getClick();

		if (clicked == null || clicked.getType() == Material.AIR) {
			return;
		}

		Validate.notNull(clickType, "Click type null.");

		for (MenuItem menuItem : menu.getItems()) {

			if (!menuItem.getItemStack().equals(clicked)) {
				continue;
			}

			menuItem.click(player, clickType);
			break;
		}
	}
}
