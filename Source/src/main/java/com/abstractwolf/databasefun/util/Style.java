package com.abstractwolf.databasefun.util;

import org.bukkit.ChatColor;

/**
 * Created by ThatAbstractWolf on 2017-06-07.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class Style {

	/**
	 * Get a formatted prefix for messages.
	 *
	 * @param prefix - the prefix
	 *
	 * @return formatted Prefix
	 */
	public static String getPrefix(String prefix) { return ChatColor.DARK_AQUA + prefix + ChatColor.DARK_RED + " ❥ " + ChatColor.GRAY; }

	/**
	 * Get a formatted prefix for messages.
	 *
	 * @param prefix - the prefix
	 * @param colour - the prefix colour
	 *
	 * @return formatted Prefix
	 */
	public static String getPrefix(String prefix, ChatColor colour) { return colour + prefix + ChatColor.DARK_RED + " ❥ " + ChatColor.GRAY; }

	/**
	 * Get the formatted line.
	 * @return formatted line.
	 */
	public static String getLine(ChatColor colour1, ChatColor colour2) {

		StringBuilder line = new StringBuilder();

		for (int i = 0; i <= 52; i++) {

			if (i % 2 == 0) {
				line.append(colour1.toString() + ChatColor.STRIKETHROUGH + "-");
			} else {
				line.append(colour2.toString() + ChatColor.STRIKETHROUGH + "-");
			}
		}

		return line.toString();
	}
}