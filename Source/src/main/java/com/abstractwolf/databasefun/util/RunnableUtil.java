package com.abstractwolf.databasefun.util;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ThatAbstractWolf on 2017-06-07.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class RunnableUtil {

	private static ExecutorService executor;

	/**
	 * Tun a task on a timer.
	 * @param plugin - the plugin.
	 * @param runnable - the runnable.
	 */
	public static void runTask(JavaPlugin plugin, Runnable runnable) { Bukkit.getScheduler().runTask(plugin, runnable); }

	/**
	 * Tun a task on a timer.
	 * @param plugin - the plugin.
	 * @param runnable - the runnable.
	 */
	public static void runTaskLater(JavaPlugin plugin, Runnable runnable, int seconds) { Bukkit.getScheduler().runTaskLater(plugin, runnable, (seconds * 20)); }


	/**
	 * Run a task async.
	 * @param runnable - the runnable to async.
	 */
	public static void runTaskAsync(Runnable runnable) { executor.execute(runnable); }

	/**
	 * Call 'onEnable()' to start executors.
	 */
	public static void startupExecutor() { executor = Executors.newCachedThreadPool(); }

	/**
	 * Call 'onDisable()' to stop executors and stop memory leaks.
	 */
	public static void shutdownExecutor() { executor.shutdown(); }
}
