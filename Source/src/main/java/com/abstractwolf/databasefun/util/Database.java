package com.abstractwolf.databasefun.util;

import com.abstractwolf.databasefun.util.callback.Callback;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by ThatAbstractWolf on 2017-06-07.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class Database {

	private HikariDataSource connectionPool;
	private String host, database, user, pass;
	private int port;

	/**
	 * Connect to the repository.. (MySQL)
	 * @param host - the host ip address to connect with.
	 * @param port - the port to connect with.
	 * @param database - the database.
	 * @param user - the username.
	 * @param pass - the password.
	 */
	public Database(String host, int port, String database, String user, String pass) {

		this.host = host;
		this.port = port;
		this.database = database;
		this.user = user;
		this.pass = pass;
	}

	/**
	 * Connect to the repository.. (MySQL)
	 */
	public void openConnection() {

			try {

				connectionPool = new HikariDataSource();
				connectionPool.setMaximumPoolSize(20);
				connectionPool.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
				connectionPool.addDataSourceProperty("serverName", host);
				connectionPool.addDataSourceProperty("port", port);
				connectionPool.addDataSourceProperty("databaseName", database);
				connectionPool.addDataSourceProperty("user", user);
				connectionPool.addDataSourceProperty("password", pass);
				connectionPool.setConnectionTimeout(3000);
				connectionPool.setValidationTimeout(1000);

				System.out.println("------------------------------[Client Information]------------------------------");
				System.out.println("Connection opened between database and client..");
				System.out.println("------------------------------[Client Information]------------------------------");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("------------------------------[Client Information]------------------------------");
				System.out.println("There was an error opening a connection between the database and the client.");
				System.out.println("------------------------------[Client Information]------------------------------");
			}
	}

	/**
	 * Send queries to the database (Prepared Statement).
	 * @param query - the query.
	 * @param callback - the callback to get the query after execution.
	 */
	public void sendPreparedStatement(String query, boolean update, boolean async, Callback<PreparedStatement> callback) {

		if (async) {
			RunnableUtil.runTaskAsync(() -> {

				Connection connection;
				PreparedStatement statement;

				try {
					connection = connectionPool.getConnection();
					statement = connection.prepareStatement(query);

					if (update) {
						statement.executeUpdate();
					} else {
						statement.execute();
					}

					callback.call(statement);
				}
				catch (SQLException e) {
					e.printStackTrace();
					callback.call(null);
					System.out.println("There was an error sending a prepared statement async..");
				}
			});
		} else {

				Connection connection;
				PreparedStatement statement;

				try {
					connection = connectionPool.getConnection();
					statement = connection.prepareStatement(query);

					if (update) {
						statement.executeUpdate();
					} else {
						statement.execute();
					}

					callback.call(statement);
				}
				catch (SQLException e) {
					e.printStackTrace();
					callback.call(null);
					System.out.println("There was an error sending a prepared statement non async..");
				}
		}
	}

	/**
	 * Check if the connection pool is closed.
	 * @return isClosed - if the pool is closed.
	 */
	public boolean isClosed() { return connectionPool.isClosed(); }

	/**
	 * Close Connection Pool
	 */
	public void closePool() {
		try {
			connectionPool.evictConnection(connectionPool.getConnection());
			connectionPool.close();
			System.out.println("Connection closed..");
		} catch (Exception e) {
			/* Ignored */
			System.out.println("Connection could not close correctly..");
		}
	}
}
