package com.abstractwolf.databasefun.util;

import com.abstractwolf.databasefun.util.callback.Callback;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by ThatAbstractWolf on 2017-06-17.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class WorldBackupUtil {

	public static void sendBackup(World world) {

		try {

			LocalDateTime now = LocalDateTime.now();
			String worldName = world.getName() + now.format(DateTimeFormatter.ofPattern("ddMMyyyy"));

			Path oldPath = Paths.get(Bukkit.getWorldContainer().getAbsolutePath() + "/" + world.getName());
			Path newPath = Paths.get(Bukkit.getWorldContainer().getAbsolutePath() + "/backups/" + worldName);

			Files.copy(oldPath, newPath, StandardCopyOption.REPLACE_EXISTING);
			System.out.println("Backup saved to - " + newPath.toString() + " with the name: " + worldName);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
