package com.abstractwolf.databasefun.util.factory;

import org.apache.commons.lang.Validate;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ThatAbstractWolf on 2017-06-13.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class SkullFactory extends Factory<ItemStack> {

	private SkullMeta skullMeta;

	/**
	 * Construct a new skull.
	 */
	public SkullFactory() {
		this.value = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		this.skullMeta = (SkullMeta) value.getItemMeta();
	}

	/**
	 * Set the display-name of the item.
	 *
	 * @param displayName - the display name.
	 */
	public final SkullFactory setDisplayName(String displayName) {
		skullMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
		return this;
	}

	/**
	 * Set the owner of a skull.
	 * @param owner - the owner
	 */
	public final SkullFactory setOwner(String owner) {
		skullMeta.setOwner(owner);
		return this;
	}

//	public final SkullFactory setCustomData(String data) {
//
//		Validate.notNull(data, "Data was null!");
//
//		GameProfile profile = new GameProfile(UUID.randomUUID(), null);
//		profile.getProperties().put("textures", new Property("textures", data));
//		Field field;
//
//		try {
//			field = skullMeta.getClass().getDeclaredField("profile");
//			field.setAccessible(true);
//			field.set(skullMeta, profile);
//		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
//			e.printStackTrace();
//		}
//
//		return this;
//	}

	/**
	 * Set the amount of an item.
	 *
	 * @param amount - the amount.
	 */
	public final SkullFactory setAmount(int amount) {
		value.setAmount(amount);
		return this;
	}

	/**
	 * Set the lore from scratch for an item. (Clears previous data).
	 *
	 * @param lore - the lore.
	 */
	public final SkullFactory setLore(String... lore) {
		List<String> finalLore = new ArrayList<>();
		Collections.addAll(finalLore, lore);
		skullMeta.setLore(finalLore);
		return this;
	}

	/**
	 * Appends a new line to the current lore.
	 *
	 * @param lore - the lore.
	 */
	public final SkullFactory appendLore(String... lore) {
		List<String> finalLore = value.getItemMeta().getLore();
		Collections.addAll(finalLore, lore);
		skullMeta.setLore(finalLore);
		return this;
	}

	/**
	 * Set the durability of an item.
	 *
	 * @param durability - the durability.
	 */
	public final SkullFactory setDurability(short durability) {
		value.setDurability(durability);
		return this;
	}

	@Override
	public ItemStack build() {
		value.setItemMeta(skullMeta);
		return value;
	}
}
