package com.abstractwolf.databasefun.util;

import java.util.Optional;

/**
 * Created by ThatAbstractWolf on 2017-06-11.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class NumberUtil {

	/**
	 * Parse a string to an integer.
	 * @param input - the integer (string)
	 *
	 * @return Integer
	 */
	public static Optional<Integer> parseInteger(String input) {
		try {
			return (Optional.of(Integer.parseInt(input)));
		} catch (NumberFormatException e) {
			return Optional.empty();
		}
	}

	/**
	 * Parse a string to a double.
	 * @param input - the double (string)
	 *
	 * @return Double
	 */
	public static Optional<Double> parseDouble(String input) {
		try {
			return (Optional.of(Double.parseDouble(input)));
		} catch (NumberFormatException e) {
			return Optional.empty();
		}
	}
}
