package com.abstractwolf.databasefun.util.factory;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ThatAbstractWolf on 2017-06-13.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class ItemFactory extends Factory<ItemStack> {

	private ItemMeta itemMeta;

	/**
	 * Construct a new item with the material for it.
	 *
	 * @param material - the material
	 */
	public ItemFactory(Material material) {
		this.value = new ItemStack(material, 1, (byte) 0);
		this.itemMeta = value.getItemMeta();
	}

	/**
	 * Construct a new item with both the material and data for it.
	 *
	 * @param material - the material
	 * @param data - the data
	 */
	public ItemFactory(Material material, byte data) {
		this.value = new ItemStack(material, 1, data);
		this.itemMeta = value.getItemMeta();
	}

	/**
	 * Set the display-name of the item.
	 *
	 * @param displayName - the display name.
	 */
	public final ItemFactory setDisplayName(String displayName) {
		itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
		return this;
	}

	/**
	 * Set the amount of an item.
	 *
	 * @param amount - the amount.
	 */
	public final ItemFactory setAmount(int amount) {
		value.setAmount(amount);
		return this;
	}

	/**
	 * Set the lore from scratch for an item. (Clears previous data).
	 *
	 * @param lore - the lore.
	 */
	public final ItemFactory setLore(String... lore) {
		List<String> finalLore = new ArrayList<>();
		Collections.addAll(finalLore, lore);
		itemMeta.setLore(finalLore);
		return this;
	}

	public final ItemFactory setLore(List<String> lore) {
		itemMeta.setLore(lore);
		return this;
	}

	/**
	 * Appends a new line to the current lore.
	 *
	 * @param lore - the lore.
	 */
	public final ItemFactory appendLore(String... lore) {
		List<String> finalLore = value.getItemMeta().getLore();
		Collections.addAll(finalLore, lore);
		itemMeta.setLore(finalLore);
		return this;
	}

	/**
	 * Set the durability of an item.
	 *
	 * @param durability - the durability.
	 */
	public final ItemFactory setDurability(short durability) {
		value.setDurability(durability);
		return this;
	}

	@Override
	public ItemStack build() {
		value.setItemMeta(itemMeta);
		return value;
	}
}
