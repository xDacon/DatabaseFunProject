package com.abstractwolf.databasefun.util.factory;

/**
 * Created by ThatAbstractWolf on 2017-06-13.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public abstract class Factory<T> {

	/** The value, could be an ItemStack etc. **/
	protected T value;

	/**
	 * Build T.
	 *
	 * @return T
	 */
	public abstract T build();
}

