package com.abstractwolf.databasefun.util.callback;

/**
 * Created by ThatAbstractWolf on 2017-06-07.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public interface DoubleCallback<K, T> {
	void call(K key, T object);
}
