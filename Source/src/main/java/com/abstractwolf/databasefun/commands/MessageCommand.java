package com.abstractwolf.databasefun.commands;

import com.abstractwolf.databasefun.DatabaseFun;
import com.abstractwolf.databasefun.base.Rank;
import com.abstractwolf.databasefun.base.User;
import com.abstractwolf.databasefun.base.commands.Command;
import com.abstractwolf.databasefun.util.Style;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Arrays;

/**
 * Created by ThatAbstractWolf on 2017-06-10.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class MessageCommand extends Command {

	public MessageCommand(DatabaseFun plugin) {

		super(plugin, "message", "Message other players on the network", Arrays.asList("msg"), Rank.PLAYER);
	}

	@Override
	public void execute(Player player, User user, String[] args) {

		if (args.length >= 2) {

			Player target = Bukkit.getPlayer(args[0]);

			if (target == null) {
				player.sendMessage(Style.getPrefix("Owl") + "We could not locate " + ChatColor.WHITE + args[0].toLowerCase() + ChatColor.GRAY + ".");
				return;
			}

			if (player == target) {
				player.sendMessage(Style.getPrefix("Owl") + "Why would you send an Owl to yourself? :'D");
				return;
			}

			StringBuilder message = new StringBuilder();

			for (int i = 1; i < args.length; i++) {
				message.append(args[i]).append(" ");
			}

			player.sendMessage(Style.getPrefix("Owl") + "You sent a new message to '" + ChatColor.WHITE + target.getName() + ChatColor.GRAY + "': " + ChatColor.WHITE + message.toString());
			target.sendMessage(Style.getPrefix("Owl") + "You got a new message from '" + ChatColor.WHITE + player.getName() + ChatColor.GRAY + "': " + ChatColor.WHITE + message.toString());
		} else {
			player.sendMessage(Style.getPrefix("Command Manager") + "Wrong arguments, " + ChatColor.YELLOW + "'/message <username> <message>'" + ChatColor.GRAY + ".");
		}
	}
}