package com.abstractwolf.databasefun.commands;

import com.abstractwolf.databasefun.DatabaseFun;
import com.abstractwolf.databasefun.base.Rank;
import com.abstractwolf.databasefun.base.User;
import com.abstractwolf.databasefun.base.commands.Command;
import com.abstractwolf.databasefun.util.NumberUtil;
import com.abstractwolf.databasefun.util.callback.DoubleCallback;
import com.abstractwolf.databasefun.util.RunnableUtil;
import com.abstractwolf.databasefun.util.Style;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Created by ThatAbstractWolf on 2017-06-10.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class DebugCommand extends Command {

	public DebugCommand(DatabaseFun plugin) {
		super(plugin, "debug", "Developer Debugs", Arrays.asList("deb"), Rank.DEV);
	}

	@Override
	public void execute(Player player, User user, String[] args) {

		if (args.length <= 1) {
			DoubleCallback<Long, Long> finishTimeCallback = (finished, finalArray) -> {

				player.sendMessage(Style.getPrefix("Debug") + "Statistics on recent count -");
				player.sendMessage(ChatColor.GRAY + "Hour(s): " + ChatColor.AQUA + TimeUnit.MILLISECONDS.toHours(finished));
				player.sendMessage(ChatColor.GRAY + "Minute(s): " + ChatColor.AQUA + TimeUnit.MILLISECONDS.toMinutes(finished));
				player.sendMessage(ChatColor.GRAY + "Second(s): " + ChatColor.AQUA + TimeUnit.MILLISECONDS.toSeconds(finished));

				getPlugin().getDatabaseInstance().sendPreparedStatement("INSERT INTO debug VALUES(DEFAULT, '" + finalArray + "');", false, true, statement -> {
					player.sendMessage(Style.getPrefix("Debug") + "Logged to database..");
				});
			};

			DoubleCallback<Boolean, Integer> callback = (finished, percentage) -> {

				if (percentage % 10 == 0) {
					player.sendMessage(Style.getPrefix("Debug") + "Complete: " + ChatColor.GREEN + percentage + ChatColor.GRAY + "%");
				}

				if (finished) {
					player.sendMessage(Style.getLine(ChatColor.LIGHT_PURPLE, ChatColor.DARK_GRAY));
					player.sendMessage(Style.getPrefix("Debug") + "Processing complete..");
					player.sendMessage(Style.getLine(ChatColor.LIGHT_PURPLE, ChatColor.DARK_GRAY));
				}
			};

			try {

				long startTime = System.currentTimeMillis();
				Optional<Integer> countToOptional = NumberUtil.parseInteger(args[0]);
				int countTo = (countToOptional.orElse(1000));

				RunnableUtil.runTaskAsync(() -> {

					for (int i = 0; i <= countTo; i++) {

						if (i == countTo) {

							long endTime = System.currentTimeMillis();

							callback.call(true, ((i * 100) / countTo));
							finishTimeCallback.call((endTime - startTime), Long.valueOf(i));
							return;
						}

						if (i >= 10000 && (i % 10000 == 0)) {
							System.out.println("Current Interval: " + i);
						}

						callback.call(false, ((i * 100) / countTo));
					}
				});
			} catch (ArrayIndexOutOfBoundsException e) {
				player.sendMessage(Style.getPrefix("Command Manager") + "Wrong arguments, " + ChatColor.YELLOW + "'/debug <number>'" + ChatColor.GRAY + ".");
			}
		}
	}
}