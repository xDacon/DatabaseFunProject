package com.abstractwolf.databasefun.commands;

import com.abstractwolf.databasefun.DatabaseFun;
import com.abstractwolf.databasefun.base.Rank;
import com.abstractwolf.databasefun.base.User;
import com.abstractwolf.databasefun.base.commands.Command;
import com.abstractwolf.databasefun.util.Style;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by ThatAbstractWolf on 2017-06-10.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class UpdateRankCommand extends Command {

	public UpdateRankCommand(DatabaseFun plugin) {
		super(plugin, "updaterank", "Update a players rank", Arrays.asList("setrank"), Rank.OWNER);
	}

	@Override
	public void execute(Player player, User user, String[] args) {

		if (args.length == 2) {

			try {

				OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
				Rank rank = Rank.valueOf(args[1].toUpperCase());

				if (!target.isOnline()) {
					if (target.hasPlayedBefore()) {
						player.sendMessage(Style.getPrefix("Online Checker") + "We could not find " + ChatColor.AQUA + target.getName() + ChatColor.GRAY + "! Updating rank whilst offline.");
						getPlugin().getDatabaseInstance().sendPreparedStatement("UPDATE users SET rank='" + rank.name().toUpperCase() + "' WHERE uuid='" + target.getUniqueId().toString() + "';", true, true, callback -> {
							player.sendMessage(Style.getPrefix("Rank Updater") + "You've updated " + ChatColor.YELLOW + target.getName() + "'s " + ChatColor.GRAY + " rank to " + rank.getFormattedPrefix(false, false) + ChatColor.GRAY + "!");
						});
					} else {
						player.sendMessage(Style.getPrefix("Online Checker") + "We could not find " + ChatColor.AQUA + target.getName() + ChatColor.GRAY + "! Have they played before?");
					}
					return;
				}

				Optional<User> targetOptional = getPlugin().getUserManager().getUser(target.getUniqueId());

				if (targetOptional.isPresent()) {

					User foundTarget = targetOptional.get();

					foundTarget.setRank(rank);
					player.sendMessage(Style.getPrefix("Rank Updater") + "You've updated " + ChatColor.YELLOW + target.getName() + "'s " + ChatColor.GRAY + " rank to " + rank.getFormattedPrefix(false, false) + ChatColor.GRAY + "!");
					target.getPlayer().sendMessage(Style.getPrefix("Rank Updater") + "Your rank has been updated to " + rank.getFormattedPrefix(false, false) + ChatColor.GRAY + "!");
				} else {
					player.sendMessage(Style.getPrefix("Rank Updater") + "There was an error processing this request!");
				}
			} catch (IllegalArgumentException e) {
				player.sendMessage(Style.getPrefix("Rank Locator") + "Could not locate a rank by the name of '" + ChatColor.DARK_PURPLE + args[1].toLowerCase() + ChatColor.GRAY + "'");
			}
		} else {
			player.sendMessage(Style.getPrefix("Command Manager") + "Wrong arguments, " + ChatColor.YELLOW + "'/updaterank <username> <rank>'" + ChatColor.GRAY + ".");
		}
	}
}