package com.abstractwolf.databasefun.commands;

import com.abstractwolf.databasefun.DatabaseFun;
import com.abstractwolf.databasefun.base.Rank;
import com.abstractwolf.databasefun.base.User;
import com.abstractwolf.databasefun.base.commands.Command;
import com.abstractwolf.databasefun.util.NumberUtil;
import com.abstractwolf.databasefun.util.Style;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by ThatAbstractWolf on 2017-06-10.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class HelpCommand extends Command {

	public HelpCommand(DatabaseFun plugin) {
		super(plugin, "help", "Displays this dialogue", Arrays.asList("h"), Rank.PLAYER);
	}

	@Override
	public void execute(Player player, User user, String[] args) {

		int page = 1; /* Default Page */
		int perPage = 5;
		int maxPage = getPages(user.getRank(), perPage);

		if (args.length <= 1) {

			if (args.length == 1) {

				Optional<Integer> pageParsed = NumberUtil.parseInteger(args[0]);

				if (!pageParsed.isPresent() || (pageParsed.isPresent() && pageParsed.get() < 1)) {
					player.sendMessage(Style.getPrefix("Command Manager") + "That is an invalid page number!");
					return;
				}

				page = pageParsed.get();
			}

			if (page > maxPage) {
				player.sendMessage(Style.getPrefix("Command Manager") + "Invalid page! Last page: " + ChatColor.DARK_RED + maxPage + ChatColor.GRAY + ".");
				return;
			}

			player.sendMessage("");
			player.sendMessage(" " + Style.getPrefix("Commands") + ChatColor.WHITE + " -= " + ChatColor.GRAY + "[" + ChatColor.YELLOW + page + ChatColor.GRAY + "/" + ChatColor.YELLOW + getPages(user.getRank(), perPage) + ChatColor.GRAY + "] " + ChatColor.WHITE + "=- - -=" + ChatColor.GRAY + " [" + user.getRank().getFormattedPrefix(false, false) + ChatColor.GRAY + "] " + ChatColor.WHITE + "=-");
			player.sendMessage("");

			if (getCommands(user.getRank(), page, perPage, true).isEmpty()) {
				player.sendMessage(ChatColor.DARK_RED + " ❥ " + ChatColor.GRAY + "You have no commands available to your rank!");
			} else {
				for (Command command : getCommands(user.getRank(), page, perPage, true)) {
					player.sendMessage(ChatColor.DARK_RED + " ❥ " + ChatColor.GRAY + command.getRequiredRank().getRankColour() + command.getCommand() + ChatColor.GRAY + " [" + command.getRequiredRank().getRankColour() + command.getRequiredRank().getRankName() + ChatColor.GRAY + "] - " + ChatColor.WHITE + command.getDescription());
				}
			}

			player.sendMessage("");
		}
	}

	/**
	 * Get the total number of pages for ranked commands.
	 * @param rank - the users rank.
	 * @param perPage - amount per page.
	 * @return Total Pages
	 */
	private int getPages(Rank rank, int perPage) {

		int commands = getPlugin().getCommandManager().getRankedHelpCommands(rank).size();
		int pages = (commands / perPage);

		if (pages < 1) {
			pages = 1;
		}

		return ((commands % perPage == 0 || commands <= perPage) ? pages : pages + 1);
	}

	/**
	 * Get all a users commands based on their rank.
	 *
	 * @param rank - the users rank.
	 * @param page - current page.
	 * @param perPage - amount per page.
	 * @return Commands based on Rank.
	 */
	private List<Command> getCommands(Rank rank, int page, int perPage, boolean sort) {

		List<Command> commands = new ArrayList<>();

		try {
			for (int i = ((page - 1) * perPage); i < (((page - 1) * perPage) + perPage); i++) {
				Command command = getPlugin().getCommandManager().getRankedHelpCommands(rank).get(i);
				commands.add(command);
			}
		} catch (IndexOutOfBoundsException e) { /* Ignored */ }

		if (sort) {
			/* Sorts in rank order (lowest rank to highest) */
			commands.sort((command1, command2) -> {

				if (command1.getRequiredRank().ordinal() == command2.getRequiredRank().ordinal()) {
					return 0;
				} else if (command1.getRequiredRank().ordinal() < command2.getRequiredRank().ordinal()) {
					return -1;
				} else {
					return 1;
				}
			});
		}

		return commands;
	}
}