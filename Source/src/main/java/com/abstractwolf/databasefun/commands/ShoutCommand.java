package com.abstractwolf.databasefun.commands;

import com.abstractwolf.databasefun.DatabaseFun;
import com.abstractwolf.databasefun.base.Rank;
import com.abstractwolf.databasefun.base.User;
import com.abstractwolf.databasefun.base.commands.Command;
import com.abstractwolf.databasefun.util.Style;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Arrays;

/**
 * Created by ThatAbstractWolf on 2017-06-10.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class ShoutCommand extends Command {

	public ShoutCommand(DatabaseFun plugin) {

		super(plugin, "shout", "Shout to players on your server", Arrays.asList("say", "me"), Rank.MOD);
	}

	@Override
	public void execute(Player player, User user, String[] args) {

		if (args.length >= 1) {

			StringBuilder message = new StringBuilder();

			for (String arg : args) {
				message.append(arg).append(" ");
			}

			Bukkit.broadcastMessage(Style.getPrefix("Shout") + ChatColor.YELLOW.toString() + ChatColor.BOLD + player.getName() + ChatColor.GRAY + ": " + ChatColor.AQUA + message.toString());
		} else {
			player.sendMessage(Style.getPrefix("Command Manager") + "Wrong arguments, " + ChatColor.YELLOW + "'/shout <message>'" + ChatColor.GRAY + ".");
		}
	}
}