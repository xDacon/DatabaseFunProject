package com.abstractwolf.databasefun;

import com.abstractwolf.databasefun.base.UserManager;
import com.abstractwolf.databasefun.base.commands.CommandManager;
import com.abstractwolf.databasefun.listener.ConnectionListener;
import com.abstractwolf.databasefun.listener.MiscListener;
import com.abstractwolf.databasefun.util.Database;
import com.abstractwolf.databasefun.util.NumberUtil;
import com.abstractwolf.databasefun.util.RunnableUtil;
import com.abstractwolf.databasefun.util.WorldBackupUtil;
import com.abstractwolf.databasefun.util.configuration.ServerConfiguration;
import com.abstractwolf.databasefun.util.menu.MenuManager;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Optional;

/**
 * Created by ThatAbstractWolf on 2017-06-07.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class DatabaseFun extends JavaPlugin implements Listener {

	private Database database;
	private ServerConfiguration serverConfiguration;

	private UserManager userManager;
	private CommandManager commandManager;

	@Override
	public void onEnable() {
		// Starts up the RunnableUtil executor instance.
		RunnableUtil.startupExecutor();
		// Sets the UserManager instance.
		userManager = new UserManager(this);
		// Sets the CommandManager
		commandManager = new CommandManager();
		// Sets Server Configuration
		serverConfiguration = new ServerConfiguration();
		getServerConfiguration().loadData();

		Optional<Integer> portOptional = NumberUtil.parseInteger(getServerConfiguration().getDatabasePort());
		int port = 3306;

		if (portOptional.isPresent()) {
			port = portOptional.get();
		}

		database = new Database(getServerConfiguration().getDatabaseHost(), port, getServerConfiguration().getDatabaseDB(), getServerConfiguration().getDatabaseUsername(), getServerConfiguration().getDatabasePassword());
		database.openConnection();

		Bukkit.getPluginManager().registerEvents(new ConnectionListener(this), this);
		Bukkit.getPluginManager().registerEvents(new MenuManager(this), this);
		Bukkit.getPluginManager().registerEvents(new MiscListener(this), this);

		Bukkit.getOnlinePlayers().forEach(online -> {
			getUserManager().addUser(online.getUniqueId());
			getUserManager().loadUser(online);
		});

		commandManager.loadCommands(this);
	}

	@Override
	public void onDisable() {

		userManager.getCachedUsers().values().forEach(user -> {
			userManager.saveUser(user.getUuid(), true);
		});

		commandManager.clearCommands();

		getServerConfiguration().saveData();

		if (getServerConfiguration().getBackupCount() == 5) {
			Bukkit.getWorlds().forEach(WorldBackupUtil::sendBackup);
		}

		RunnableUtil.shutdownExecutor();
		database.closePool();
	}

	public Database getDatabaseInstance() { return this.database; }

	public ServerConfiguration getServerConfiguration() {
		return serverConfiguration;
	}

	public UserManager getUserManager() {
		return userManager;
	}

	public CommandManager getCommandManager() {
		return commandManager;
	}
}
