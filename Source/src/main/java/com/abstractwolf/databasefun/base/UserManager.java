package com.abstractwolf.databasefun.base;

import com.abstractwolf.databasefun.DatabaseFun;
import com.abstractwolf.databasefun.util.Database;
import com.abstractwolf.databasefun.util.RunnableUtil;
import com.abstractwolf.databasefun.util.Style;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2017-06-07.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class UserManager {

	private DatabaseFun plugin;
	private Map<UUID, User> cachedUsers;

	public UserManager(DatabaseFun plugin) {
		this.plugin = plugin;
		this.cachedUsers = new HashMap<>();
	}

	public User addUser(UUID uuid) {
		User user = new User(uuid);
		cachedUsers.put(uuid, user);
		return user;
	}

	public void removeUser(UUID uuid) {
		cachedUsers.remove(uuid);
	}

	public Optional<User> getUser(UUID uuid) {
		return (cachedUsers.containsKey(uuid) ? Optional.of(cachedUsers.get(uuid)) : Optional.empty());
	}

	public Map<UUID, User> getCachedUsers() {
		return cachedUsers;
	}

	// CREATE TABLE IF NOT EXISTS users (ID INT NOT NULL AUTO_INCREMENT, uuid VARCHAR(45), rank VARCHAR(50), PRIMARY KEY(ID));

	public void loadUser(Player player) {

		Database database = plugin.getDatabaseInstance();

		database.sendPreparedStatement("SELECT * FROM users WHERE uuid='" + player.getUniqueId().toString() + "';", false, true, callback -> {

			if (callback == null) {
				RunnableUtil.runTaskLater(plugin, () -> player.kickPlayer(Style.getPrefix("Database") + "There was an error sending the query to the database, please rejoin or report to an admin+!"), 1);
				return;
			}

			try {

				ResultSet set = callback.getResultSet();
				User user = (plugin.getUserManager().getUser(player.getUniqueId()).isPresent() ? plugin.getUserManager().getUser(player.getUniqueId()).get() : null);

				if (user == null) {
					RunnableUtil.runTaskLater(plugin, () -> player.kickPlayer(Style.getPrefix("Database") + "There was an error setting your data, please rejoin or report to an admin+!"), 1);
					return;
				}

				if (set.next()) {
					user.setId(set.getInt("id"));
					user.setRank(Rank.valueOf(set.getString("rank").toUpperCase()));
				}

				player.sendMessage(Style.getPrefix("User Manager") + "User Information -");
				player.sendMessage(ChatColor.YELLOW + "UUID: " + ChatColor.GRAY + user.getUuid().toString());
				player.sendMessage(ChatColor.YELLOW + "Rank: " + ChatColor.GRAY + user.getRank().name().toUpperCase());
			} catch (SQLException e) {
				e.printStackTrace();
				RunnableUtil.runTaskLater(plugin, () -> player.kickPlayer(Style.getPrefix("Database") + "There was an error adding your data to the cache, please rejoin or report to an admin+!"), 1);
			}
		});
	}

	public void saveUser(UUID uuid, boolean shutdown) {

		Database database = plugin.getDatabaseInstance();
		User user = null;

		if (getUser(uuid).isPresent()) {
			user = getUser(uuid).get();
		}

		if (user == null) {
			System.out.println("There was an error saving " + uuid.toString() + "'s data..");
			return;
		}

		if (!shutdown) {
			if (user.getId() == -1) {
				database.sendPreparedStatement("INSERT INTO users (uuid, rank) VALUES('" + uuid.toString() + "', '" + user.getRank().name().toUpperCase() + "');", false, true, callback -> {});
			} else {
				database.sendPreparedStatement("UPDATE users SET uuid='" + uuid.toString() + "', rank='" + user.getRank().name().toUpperCase() + "' WHERE id='" + user.getId() + "';", true, true, callback -> {});
			}
		} else {
			if (user.getId() == -1) {
				database.sendPreparedStatement("INSERT INTO users (uuid, rank) VALUES('" + uuid.toString() + "', '" + user.getRank().name().toUpperCase() + "');", false, false, callback -> {});
			} else {
				database.sendPreparedStatement("UPDATE users SET uuid='" + uuid.toString() + "', rank='" + user.getRank().name().toUpperCase() + "' WHERE id='" + user.getId() + "';", true, false, callback -> {});
			}
		}
	}
}
