package com.abstractwolf.databasefun.base.commands;

import com.abstractwolf.databasefun.DatabaseFun;
import com.abstractwolf.databasefun.base.Rank;
import com.abstractwolf.databasefun.base.User;
import com.abstractwolf.databasefun.util.Style;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by ThatAbstractWolf on 2017-06-11.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public abstract class Command extends BukkitCommand {

	private DatabaseFun plugin;

	private String command, description;
	private List<String> aliases;
	private Rank requiredRank;

	public Command(DatabaseFun plugin, String command, String description, List<String> aliases, Rank requiredRank) {

		super(command);

		this.plugin = plugin;

		this.command = command;
		this.description = description;
		this.aliases = aliases;
		this.requiredRank = requiredRank;

		register();
	}

	public String getCommand() {
		return command;
	}

	public List<String> getAliases() {
		return aliases;
	}

	public Rank getRequiredRank() {
		return requiredRank;
	}

	public DatabaseFun getPlugin() {
		return plugin;
	}

	public abstract void execute(Player player, User user, String[] args);

	private void register() {

		try {

			Field field = Bukkit.getServer().getClass().getDeclaredField("commandMap");
			field.setAccessible(true);
			CommandMap commandMap = (CommandMap) field.get(Bukkit.getServer());
			setAliases(aliases);
			setDescription(description);
			commandMap.register(command, this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean execute(CommandSender sender, String commandLabel, String[] args) {

		if (sender instanceof Player) {

			Player player = (Player) sender;

			Optional<User> user = plugin.getUserManager().getUser(player.getUniqueId());

			if (user.isPresent()) {
				User foundPlayer = user.get();

				if (!foundPlayer.getRank().hasRank(requiredRank, false)) {
					player.sendMessage(Style.getPrefix("Permission") + "You require the rank " + ChatColor.DARK_GRAY + "[" + requiredRank.getFormattedPrefix(false, false) + ChatColor.DARK_GRAY + "]");
					return false;
				}

				execute(player, user.get(), args);
			} else {
				player.sendMessage(Style.getPrefix("User Manager") + "Your player data could not be loaded.. please relog.");
			}
		}
		return false;
	}
}
