package com.abstractwolf.databasefun.base.commands;

import com.abstractwolf.databasefun.DatabaseFun;
import com.abstractwolf.databasefun.base.Rank;
import com.abstractwolf.databasefun.commands.DebugCommand;
import com.abstractwolf.databasefun.commands.HelpCommand;
import com.abstractwolf.databasefun.commands.MessageCommand;
import com.abstractwolf.databasefun.commands.ShoutCommand;
import com.abstractwolf.databasefun.commands.UpdateRankCommand;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThatAbstractWolf on 2017-06-11.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class CommandManager {

	private List<Command> loadedCommands;

	public CommandManager() {
		this.loadedCommands = new ArrayList<>();
	}

	public void addCommand(Command command) {
		loadedCommands.add(command);
	}

	public void clearCommands() {
		loadedCommands.clear();
	}

	public List<Command> getRankedHelpCommands(Rank rank) {

		List<Command> commands = new ArrayList<>();

		for (Command command : loadedCommands) {
			if (command.getRequiredRank().hasRank(rank, true)) {
				commands.add(command);
			}
		}

		return commands;
	}

	public List<Command> getLoadedCommands() {
		return loadedCommands;
	}

	public void loadCommands(DatabaseFun plugin) {
		addCommand(new HelpCommand(plugin));
		addCommand(new MessageCommand(plugin));
		addCommand(new ShoutCommand(plugin));
		addCommand(new DebugCommand(plugin));
		addCommand(new UpdateRankCommand(plugin));
	}
}
