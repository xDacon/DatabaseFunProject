package com.abstractwolf.databasefun.base;

import java.util.UUID;

/**
 * Created by ThatAbstractWolf on 2017-06-07.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public class User {

	private int id = -1;
	private UUID uuid;
	private Rank rank = Rank.PLAYER;

	public User(UUID uuid) {
		this.uuid = uuid;
	}

	public int getId() {
		return id;
	}

	public UUID getUuid() {
		return uuid;
	}

	public Rank getRank() {
		return rank;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setRank(Rank rank) {
		this.rank = rank;
	}
}
