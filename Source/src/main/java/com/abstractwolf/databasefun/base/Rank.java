package com.abstractwolf.databasefun.base;

import org.bukkit.ChatColor;

/**
 * Created by ThatAbstractWolf on 2017-06-07.
 * © ThatAbstractWolf, DatabaseFunProject 2017.
 */
public enum Rank {

	PLAYER("Player", ChatColor.GRAY),
	MOD("Mod", ChatColor.GREEN),
	SNR_MOD("Sr.Mod", ChatColor.DARK_GREEN),
	ADMIN("Admin", ChatColor.RED),
	DEV("Dev", ChatColor.RED),
	OWNER("Owner", ChatColor.DARK_RED);

	private String rankName;
	private ChatColor rankColour;

	Rank(String rankName, ChatColor rankColour) {
		this.rankName = rankName;
		this.rankColour = rankColour;
	}

	public String getRankName() {
		return rankName;
	}

	public ChatColor getRankColour() {
		return rankColour;
	}

	public String getFormattedPrefix(boolean uppercase, boolean bold) {

		if (bold) {
			return rankColour.toString() + ChatColor.BOLD + (uppercase ? rankName.toUpperCase() : rankName);
		}

		return rankColour.toString() + (uppercase ? rankName.toUpperCase() : rankName);
	}

	public boolean hasRank(Rank rank, boolean inverse) {
		return (!inverse ? this.ordinal() >= rank.ordinal() : this.ordinal() <= rank.ordinal());
	}
}
